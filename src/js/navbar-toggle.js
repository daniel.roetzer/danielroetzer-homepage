document.getElementById("navbar-burger-toggle").addEventListener("click", function () {
  // Toggle class on burger button and navbar itself
  this.classList.toggle("is-active");
  document.getElementById(this.dataset.target).classList.toggle("is-active");
});