var { src, dest, series, watch, parallel } = require("gulp");
var sass = require("gulp-sass");
var rename = require("gulp-rename");
var postcss = require("gulp-postcss");
var autoprefixer = require("autoprefixer");
var cssnano = require("cssnano");
var sourcemaps = require("gulp-sourcemaps");
var browserSync = require("browser-sync").create();
var uglify = require("gulp-uglify");

// Use node-sass library for faster sass compiling
sass.compiler = require("node-sass");


// Compile, Autoprefix, Minify Sass to CSS
function compileStyles() {
  return src("src/scss/index.scss")
    .pipe(sourcemaps.init())
    .pipe(sass().on("error", sass.logError))
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(rename("main.min.css"))
    .pipe(sourcemaps.write("."))
    .pipe(dest("public/css/"))
    .pipe(browserSync.stream());
}

function compileScripts() {
  return src("src/js/**/*.js")
    .pipe(uglify())
    .pipe(rename("main.min.js"))
    .pipe(dest("public/js/"))
    .pipe(browserSync.stream());
}


// Serve files as a server (with hot reload)
function serve() {
  browserSync.init({
    server: "./public"
  });

  watch("src/scss/**/*.scss", series(compileStyles));
  watch("src/js/**/*.js", series(compileScripts));
  watch("public/**/*.html").on("change", browserSync.reload);
}


// Export tasks
exports.serve = series(parallel(compileStyles, compileScripts), serve);
exports.default = parallel(compileStyles, compileScripts);

// Build command for Netlify.com
exports.build = parallel(compileStyles, compileScripts);
